# EC2 Services @ 18.194.101.155 #t2.medium (2cpu,4gb ram)

Docker-Mapserver 7
Docker-Mapcache
Docker-PostGIS/Postgres 2.4/10.3 (see docs in repo - Docker-postgres)



## DOCKER MAPSERVER:
******************

1) If not alredy installed - install docker using this guide (if applicable): https://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-basics.html#install_docker

2) Create folder "docker" under ec2-user homefolder <- this will be root for all docker-images...

3) Clone docker image for Mapserver - docs here -> https://bitbucket.org/kyv_geodata/docker-mapserver

```
git clone https://github.com/srounet/docker-mapserver

cd docker-mapserver

docker build -t mapserver
```

4) Run docker:

```
sudo docker run --restart always -d -p 8081:80 -v /usr/local/mapserver:/maps --name mapserver mapserver
```

5) Make sure port 8081 mapserver and 8082 is added to AWS security groups - use AWS console.

6) Check if mapserver is responding:

```
http://localhost:8081/cgi-bin/mapserv or http://18.194.101.155:8081/cgi-bin/mapserv 
```

7) Config mapserver:

Create or copy a dummy mapfile and data folder to /usr/local/mapserver

8) Check again with a query to see all ok:

```
http://18.194.101.155:8081/cgi-bin/mapserv?map=/maps/mapfile.map&layer=country&mode=map
```

9) Place tetthetsplott .tif under data folder

10) Example mapfile:

NOTE!: Regarding Postgres connectionstring and "wms_onlineresource" -->   Use public IP wherever applicable instead of "localhost" or else you're screwed.

********************************************************************************************************
```
# http://18.194.101.155:8081/cgi-bin/mapserv?map=/maps/mapfile.map&service=wms&version=1.1.1&request=GetCapabilities
MAP
  # CONFIG  "MS_ERRORFILE" "stderr"
  # DEBUG 5
  NAME           "WMS"
  IMAGETYPE      PNG
  EXTENT         -179 -89 179 89
  SIZE           500 500
  MAXSIZE        120000
  SHAPEPATH      "./data"
  IMAGECOLOR     255 255 255

  WEB
    METADATA
      "wms_title"                         "Norwegian Coastal Administration WMS, Experimental"
      "wms_onlineresource"                "http://18.194.101.155:8081/cgi-bin/mapserv?map=/maps/mapfile.map&"
      "wms_srs"                           "EPSG:4326 EPSG:32633"
      "wms_enable_request"                "*"
      "wms_encoding"                      "UTF-8"
      "wms_accessconstraints"             "none"
      "wms_contactperson"                 "Norwegian Coastal Administration, Geodataservices"
      "wms_contactorganization"           "Norwegian Coastal Administration, Geodataservices"
      "wms_contactposition"               "None"
      "wms_contactelectronicmailaddress"  "geodatatjenesten@kystverket.no"
      "wms_accessconstraints"             "none"
      "wms_fees"                          "none"
      "wms_abstract"                      "Exprimental service"
      "wms_contactfacsimiletelephone"     "None"
      "wms_contactvoicetelephone"         "None"
      "wms_keywordlist"                   "AIS,Density,Raster,Shiptraffic,Shipping"
    END
  END

  PROJECTION
    "init=epsg:4326"
  END


  # Start of LAYER DEFINITIONS ---------------------------------------------
  LAYER # Start country polygon layer here
    NAME         country
    METADATA
      "wms_title"    "country"   ##required
    END
    DATA         cntry02
    STATUS       ON
    TYPE         POLYGON
    CLASS
      NAME       "Landareas"
      STYLE
        COLOR        232 232 232
        OUTLINECOLOR 32 32 32
      END
    END
  END # End country polygon layer here

    LAYER
        METADATA
            "wms_title"     "rasterkart"
            "wms_abstract"      "rasterkart based on AIS"
        END
        DATA "raster.tif"
        #EXTENT -241000 6437500 1283000 7961500
        NAME "rasterkart"
        TYPE raster
        OFFSITE 255 255 255
        PROJECTION "init=epsg:32633" END
    END

    LAYER
        METADATA
            "wms_title"     "hex"
            "wms_abstract"  "hex"
        END
        NAME "hex"
        TYPE POLYGON
        CONNECTIONTYPE postgis
        CONNECTION "user=postgres password=_sj0r0k_ dbname=kyv host=18.194.101.155 port=5432"
        DATA "the_geom from grid.hex_grid_0_5 using SRID=4326 using unique gid"
        COMPOSITE
            OPACITY 50
        END # COMPOSITE
        CLASS
            STYLE
                COLOR        255 255 255
                OUTLINECOLOR 32 32 32
                ANTIALIAS true
            END
        END
        PROJECTION "init=epsg:4326" END
    END


  # End of LAYER DEFINITIONS ---------------------------------------------
END

```
*************************************************************************************************

## DOCKER MAPCACHE:
*****************



1) Clone docker image for Mapcache: 

```
git clone https://github.com/camptocamp/docker-mapcache.git

cd docker-mapcache

docker build -t mapcache .

NOTE: In order for this to work one has to make sure that the mapcache.xml exists in /usr/local/mapserver

docker run --restart=always -d -p 8082:80 -v /usr/local/mapserver/cache:/var/sig/tiles -v /usr/local/mapserver/mapcache.xml:/mapcache/mapcache.xml --name mapcache mapcache
```


Same as from here:

.[https://bitbucket.org/kyv_geodata/docker-mapserver](https://bitbucket.org/kyv_geodata/docker-mapserver)









